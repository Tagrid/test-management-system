const express = require('express');
const Handlebars = require('handlebars');
const path = require('path');
const exphbs = require('express-handlebars');
const { allowInsecurePrototypeAccess } = require('@handlebars/allow-prototype-access');
const flash = require('connect-flash');
const session = require('express-session');
const methodOverride = require('method-override')
const bodyParser = require('body-parser');
const passport = require('passport');
const mongoose = require('mongoose');

const app = express();

//Load routes
const issues = require('./routes/issues');
const users = require('./routes/users');
const projects = require('./routes/projects');

//Passport config
require('./config/passport')(passport);

mongoose.Promise = global.Promise

//Connect to mongoose
mongoose.connect('mongodb://localhost/test-management-system', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => {
        console.log('MongoDB: Connected...')
    })
    .catch(err => console.log(err));

//Body-parer middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Static folder
app.use(express.static(path.join(__dirname, 'public')));

//MethodOverride middleare
app.use(methodOverride('_method'));

//Connect-flash middleware
app.use(flash());

//Session middleware
app.use(session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

//Passport middleware
app.use(passport.initialize());
app.use(passport.session());

//Global variables
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.err_msg = req.flash('err_msg');
    res.locals.error = req.flash('error');
    res.locals.user = req.user || null;
    next();
});

//Handlebars middleware
app.engine('handlebars', exphbs({
    defaultLayout: 'main',
    handlebars: allowInsecurePrototypeAccess(Handlebars)
}));
app.set('view engine', 'handlebars');

//How middleware works
app.use(function (req, res, next) {
    // console.log(Date.now());
    next();
});

//Index Route
app.get('/', (req, res) => {
    const title = 'Welcome'
    res.render('index', {
        title: title
    });

});

//About
app.get('/about', (req, res) => {
    res.render('about')
});

//Use routes
app.use('/issues', issues);
app.use('/users', users);
app.use('/projects', projects);

const port = 5000;

app.listen(port, () => {
    console.log(`Server started on ${port}`);
});