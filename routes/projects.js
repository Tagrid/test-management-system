const express = require('express');
const mongoose = require('mongoose')
const router = express.Router();
const fs = require('fs');
const { ensureAuthenticated } = require('../helpers/auth')

//Load project model
require('../models/Project');
const Project = mongoose.model('projects');

//Load issues model
require('../models/Issue');
const Issue = mongoose.model('issues');


//Load users model
require('../models/User');
const User = mongoose.model('users');

//Load testcases model
require('../models/TestCase');
const TestCase = mongoose.model('testCases');

//Projects Index page
router.get('/', ensureAuthenticated, (req, res) => {
    Project.find().or([{ 'user.id': req.user.id }, { 'people.id': req.user.id }])
        .sort({ date: 'desc' })
        .then(projects => {
            res.render('projects/index', {
                projects: projects
            });
        });
});

//Add project
router.get('/add', ensureAuthenticated, (req, res) => {
    res.render('projects/add')
});

//Test Case Details
router.get('/testCaseDetails/:id', ensureAuthenticated, (req, res) => {
    TestCase.findOne({ _id:req.params.id })
    .then(tCase => {
        res.render('projects/testCaseDetails', {
            id: tCase.id,
            title: tCase.title,
            status: tCase.status,
            steps: tCase.steps,
            testPlanId: tCase.testPlan,
            projectId: tCase.project,
            type: tCase.issueType,
            owner: tCase.reporterName
        });
    });
});

//Add test cases page
router.get('/:id/addCase', ensureAuthenticated, (req, res) => {

    Issue.findOne({ _id : req.params.id })
    .then(testPlan => {
        res.render(`projects/addCase`, {
            id: testPlan.id,
            title: testPlan.title,
            prId: testPlan.project
        })
    })
});

//Get test case details
router.get('/changeCaseStatus/:id', ensureAuthenticated, (req, res) => {

    TestCase.findOne({ _id: req.params.id })
    .then(tCase => {
        res.render(`projects/changeCaseStatus`, {
            id: req.params.id,
            status: tCase.status
        })
    });

});

//People page
router.get(`/people/:id`, ensureAuthenticated, (req, res) => {

    Project.findOne({ _id: req.params.id })
        .then(project => {
            res.render('projects/people', {
                id: project.id,
                name: project.user.name,
                email: project.user.email,
                people: project.people
            });
        })

});


//Project Details
router.get(`/details/:id`, ensureAuthenticated, (req, res) => {

    Project.findOne({ _id: req.params.id })
        .then(project => {
            res.render('projects/details', { id: project.id, title: project.title, details: project.details });
        })

});

//Project issues
router.get(`/issues/:id`, ensureAuthenticated, (req, res) => {

    Issue.find({ project: req.params.id, issueType: 'Bug' })
        .then(issues => {

            res.render('projects/issues', { issues: issues, prId: req.params.id })

        })

});

//Project requirements (user stories)
router.get(`/requirements/:id`, ensureAuthenticated, (req, res) => {

    Issue.find({ project: req.params.id, issueType: 'User Story' })
        .then(issues => {

            res.render('projects/issues', { issues: issues, prId: req.params.id })

        })

});

//Project test plans
router.get(`/testPlans/:id`, ensureAuthenticated, (req, res) => {

    Issue.find({ project: req.params.id, issueType: 'Test Plan' })
        .then(issues => {

            res.render('projects/testPlans', { issues: issues, prId: req.params.id })

        })

});

//Project test plan details
router.get(`/testPlanDetails/:id`, ensureAuthenticated, (req, res) => {

    Issue.findOne({ _id : req.params.id })
        .then(testPlan => {

            TestCase.find({ testPlan : testPlan.id})
            .then(testCases => {
                res.render('projects/testPlanDetails', {
                    id: testPlan.id,
                    testPlanProject: testPlan.project,
                    title: testPlan.title,
                    owner: testPlan.userName,
                    userName: testPlan.userAssignedName,
                    status: testPlan.status,
                    type: testPlan.issueType,
                    testCases: testCases
                    });

            })

        })

});

//Add people page
router.get(`/addPeople/:id`, ensureAuthenticated, (req, res) => {
    Project.findOne({
        _id: req.params.id
    })
        .then(project => {

            res.render('projects/addPeople', {
                project: project
            });

        });
});


//Edit projects
router.get('/edit/:id', ensureAuthenticated, (req, res) => {
    Project.findOne({
        _id: req.params.id
    })
        .then(project => {


            if (project.user.id !== req.user.id) {

                req.flash('err_msg', 'Ooops, something`s got wrong!');
                res.redirect('/projects')

            } else {

                res.render('projects/edit', {
                    project: project
                });
            }


        });

});

//Process form
router.post('/', ensureAuthenticated, (req, res) => {
    let errors = [];

    if (!req.body.title) {
        errors.push({ text: 'Please add a title' })
    }
    if (!req.body.details) {
        errors.push({ text: 'Please add some details' })
    }

    if (errors.length > 0) {
        res.render('projects/add', {
            errors: errors,
            title: req.body.title,
            details: req.body.details
        });
    } else {
        const newUser = {
            title: req.body.title,
            details: req.body.details,
            user: {
                id: req.user.id,
                name: req.user.name,
                email: req.user.email,
                role: 'Owner'
            }
        }
        new Project(newUser)
            .save()
            .then(project => {
                req.flash('success_msg', 'Project Added');
                res.redirect('/projects');
            });
    }
});


//Add people to the project

router.put('/:id/addUser', ensureAuthenticated, (req, res) => {

    Project.findById(req.params.id)
        .then(project => {

            User.findOne({ email: req.body.email })
                .then(user => {

                    if (!user) {
                        req.flash('err_msg', 'User does not exist!')
                        res.redirect('/')
                    } else {

                        project.people.push({
                            id: user.id,
                            prId: req.params.id,
                            name: user.name,
                            email: req.body.email,
                            role: req.body.role
                        });

                        user.projects.push({
                            id: project.id,
                            role: req.body.role
                        });

                        project.save()
                            .then(() => {
                                user.save();
                            })
                            .then(() => {

                                req.flash('success_msg', 'User Added');
                                res.redirect(`/projects/people/${req.params.id}`)

                            });

                    }

                });

        });

});


//Edit Form process
router.put('/:id', ensureAuthenticated, (req, res) => {
    Project.findOne({
        _id: req.params.id
    })
        .then(project => {
            //New values
            project.title = req.body.title;
            project.details = req.body.details;

            project.save()
                .then(project => {
                    req.flash('success_msg', 'Project Updated');
                    res.redirect('/projects')
                });
        });
});

//Add test case
router.post('/project/:prId/testPlan/:id/addCase', ensureAuthenticated, (req, res) => {

    let steps = [];
    let addedSteps = req.body.step;
    let expectedResults = req.body.expected;
    let counter = 1;
    console.log(expectedResults)
    console.log(addedSteps)

    if(Array.isArray(addedSteps)){
    addedSteps.forEach((step, index) => {

        steps.push( {
            step: step,
            expected: expectedResults[index]
        });
    });
    }else{
        steps.push({
            step: addedSteps,
            expected: expectedResults
        });
    }


    const newCase = {
        title: req.body.title,
        steps: steps,
        reporter: req.user.id,
        reporterName: req.user.name,
        status: 'Open',
        testPlan: req.params.id,
        project: req.params.prId,
        issueType: 'Test Case',       
    };

    new TestCase(newCase)
    .save()
    .then(tCase => {
        req.flash('success_msg', 'Test Case Added');
        res.redirect(`/projects/testPlanDetails/${req.params.id}`);
    });

});

//Change case status
router.put('/testCase/:id', ensureAuthenticated, (req, res) => {

    TestCase.findOne({ _id:req.params.id })
    .then(tCase => {

        tCase.status = req.body.caseStatus;

        tCase.save()
        .then(() => {
            res.redirect(`/projects/testCaseDetails/${tCase.id}`)
        })

    });

});

//Delete project
router.delete('/:id', ensureAuthenticated, (req, res) => {

    User.find({})
        .then(users => {

            users.forEach(user => {

                let projectToDelete = user.projects.find(pr => pr.id === req.params.id)

                if (projectToDelete) {
                    user.projects.pull(projectToDelete);
                    user.save();
                }
            });
        });

    Project.deleteOne({ _id: req.params.id })
        .then(() => {
            req.flash('success_msg', 'Project Removed');
            res.redirect('/projects')
    });
})


//Remove people from project
router.put('/removePerson/:id/project/:prId', ensureAuthenticated, (req, res) => {

    Project.findOne({ _id: req.params.prId })
        .then(project => {

            let personToRemove = project.people.find(person => person.id = req.params.id);
            project.people.pull(personToRemove);
            project.save()
                .then(() => {
                    res.redirect(`/projects/people/${req.params.prId}`);
                })

        });

    User.findOne({ _id: req.params.id })
        .then(user => {

            let projectToDelete = user.projects.find(project => project.id = req.params.prId);
            user.projects.pull(projectToDelete);
            user.save();

        })

});
module.exports = router;