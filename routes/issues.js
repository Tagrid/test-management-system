const express = require('express');
const mongoose = require('mongoose')
const router = express.Router();
const { ensureAuthenticated } = require('../helpers/auth')

//Load issue model
require('../models/Issue');
const Issue = mongoose.model('issues');

//Load project model
require('../models/Project');
const Project = mongoose.model('projects');

//Load user model
require('../models/User');
const User = mongoose.model('users');

//Issues Index page
router.get('/', ensureAuthenticated, (req, res) => {
    Issue.find({ userAssigned: req.user.id })
        .sort({ date: 'desc' })
        .then(issues => {
            let openIssues = [];

            issues.forEach(issue => {

                if (issue.status === 'Open' || issue.status === 'In Progress' || issue.status === 'In Code Review' || issue.status === 'In Testing') {
                    openIssues.push(issue)
                }

            });
            res.render('issues/index', {
                issues: openIssues
            });
        });
});

//Issues get reassign page
router.get('/reassign/:id', ensureAuthenticated, (req, res) => {

    Issue.findOne({ _id: req.params.id })
        .then(issue => {
            User.findOne({ _id: issue.userAssigned })
                .then(user => {
                    res.render('issues/reassign', {
                        title: issue.title,
                        id: req.params.id,
                        details: issue.details,
                        issueType: issue.issueType,
                        status: issue.status,
                        userName: user.name,
                        owner: issue.userName,
                        userId: issue.user
                    });
                })
        })

});

//Add issue
router.get('/add', ensureAuthenticated, (req, res) => {
    res.render('issues/add')
});

//Issue Details
router.get('/details/:id', ensureAuthenticated, (req, res) => {
    Issue.findOne({ _id: req.params.id })
        .then(issue => {
            User.findOne({ _id: issue.userAssigned })
                .then(user => {
                    res.render('issues/details', {
                        title: issue.title,
                        id: req.params.id,
                        details: issue.details,
                        issueType: issue.issueType,
                        status: issue.status,
                        userName: user.name,
                        owner: issue.userName,
                        userId: issue.user
                    });
                })
        })
});

//Edit issues
router.get('/edit/:id', ensureAuthenticated, (req, res) => {
    Issue.findOne({
        _id: req.params.id
    })
        .then(issue => {


            if (issue.user !== req.user.id) {

                req.flash('err_msg', 'Ooops, something`s got wrong!');
                res.redirect('/issues')

            } else {

                res.render('issues/edit', {
                    issue: issue
                });
            }


        });

});

//Process form
router.post('/', ensureAuthenticated, (req, res) => {

    Project.findOne({ title: req.body.project })
        .or([{ 'user.id': req.user.id }, { 'people.id': req.user.id }])
        .then(projectExist => {

            if (!projectExist) {
                req.flash('err_msg', 'Project does not exist!');
                res.redirect('issues/add')

            } else {

                User.findOne({ name: req.body.userAssigned })
                    .then(userFound => {

                        if (!userFound) {
                            req.flash('err_msg', 'User does not exist!');
                            res.redirect('issues/add');
                        } else {
                            let userIsAssigned = userFound.projects.find(project => project.id = projectExist.id)

                            if (!userIsAssigned || projectExist.user.id !== req.user.id) {
                                req.flash('err_msg', 'User is not assigned to this project!');
                                res.redirect('issues/add');
                            } else {
                                const newIssue = {
                                    title: req.body.title,
                                    details: req.body.details,
                                    user: req.user.id,
                                    userName: req.user.name,
                                    project: projectExist.id,
                                    issueType: req.body.issueType,
                                    userAssigned: userFound.id,
                                    userAssignedName: userFound.name,
                                    status: 'Open'
                                }

                                new Issue(newIssue)
                                    .save()
                                    .then(issue => {
                                        req.flash('success_msg', 'Issue Added');
                                        res.redirect('/issues');
                                    });

                            }

                        }

                    });
            }
        })
});

//Edit Form process
router.put('/:id', ensureAuthenticated, (req, res) => {
    Issue.findOne({
        _id: req.params.id
    })
        .then(issue => {
            //New values
            issue.title = req.body.title;
            issue.details = req.body.details;

            issue.save()
                .then(issue => {
                    req.flash('success_msg', 'Issue Updated');
                    res.redirect('/issues')
                });
        });
});

//Reassign Issue
router.put('/reassign/issue/:id', ensureAuthenticated, (req, res) => {
    Issue.findOne({
        _id: req.params.id
    })
        .then(issue => {
            User.findOne({ name: req.body.userAssigned })
                .then(userFound => {
                    if (!userFound) {
                        req.flash('err_msg', 'User does not exist!');
                        res.redirect(`/issues/details/${issue.id}`);
                    } else {
                        Project.findOne({ _id: issue.project })
                        .then(projectExist => {
                        let userIsAssigned = userFound.projects.find(project => project.id = projectExist.id)

                        if (!userIsAssigned || projectExist.user.id !== req.user.id) {
                            req.flash('err_msg', 'User is not assigned to this project!');
                            res.redirect(`/issues/details/${issue.id}`);
                        } else {
                            //New values
                            issue.status = req.body.status;
                            issue.userAssigned = userFound.id;

                            issue.save()
                                .then(issue => {
                                    req.flash('success_msg', 'Issue Updated');
                                    res.redirect(`/issues/details/${issue.id}`)
                                });
                        }

                    });

                    }


            });
        });
});

//Delete issue
router.delete('/:id', ensureAuthenticated, (req, res) => {
    Issue.deleteOne({ _id: req.params.id })
        .then(() => {
            req.flash('success_msg', 'Issue Removed');
            res.redirect('/issues')
        });
});

module.exports = router;