const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create schema
const ProjectSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    details:{
        type: String,
        required: true
    },
    user:{
        type: Object,
        required: true
    },
    people:{
        type: Array,
        required: false
    },
    tickets: {
        type: Object,
        required: false
    },
    date:{
        type: Date,
        default: Date.now
    }
});

mongoose.model('projects', ProjectSchema);