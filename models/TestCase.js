const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create schema
const TestCaseSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    steps:{
        type: Array,
        required: true
    },
    reporter:{
        type: String,
        required: true
    },
    reporterName:{
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    testPlan: {
        type: String,
        required: true
    },
    project: {
        type: String,
        required: true
    },
    issueType: {
        type: String,
        required: false
    },
    date:{
        type: Date,
        default: Date.now
    }
});

mongoose.model('testCases', TestCaseSchema);