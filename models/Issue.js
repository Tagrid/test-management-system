const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Create schema
const IssueSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    details:{
        type: String,
        required: true
    },
    user:{
        type: String,
        required: true
    },
    userName:{
        type: String,
        required: true
    },
    userAssigned: {
        type: String,
        required: true
    },
    userAssignedName: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    },
    project: {
        type: String,
        required: true
    },
    issueType: {
        type: String,
        required: true
    },
    date:{
        type: Date,
        default: Date.now
    }
});

mongoose.model('issues', IssueSchema);